<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 16px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                line-height: 20px: 
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .api-data {
                font-size: 12px;
                font-weight: normal;
            }
        </style>
    </head>
    <body>
        <div>

            <div class="content">
                <div class="title m-b-md flex-center position-ref">
                    SocialOut API
                </div>

                <div class="links">
                    <a href="#">/api/places</a> <br/>
                    <div class="api-data">
                        {"places":[{"id":1,"name":"Hotel 1","description":"Hotel 1 description","theme":"hotel","destination_id":10,"lat":"12.3346","log":"33.4455","created_at":"2017-01-17 00:34:35","updated_at":"2017-01-17 01:58:19"},{"id":2,"name":"Restaurant 1","description":"Restaurant 1 description","theme":"restaurant","destination_id":10,"lat":"12.3346","log":"33.4455","created_at":"2017-01-17 00:35:56","updated_at":"2017-01-17 00:35:56"},{"id":3,"name":"Pub 1","description":"Pub 1 description","theme":"pub","destination_id":10,"lat":"12.3346","log":"33.4455","created_at":"2017-01-17 00:35:56","updated_at":"2017-01-17 01:59:42"},{"id":4,"name":"Night Club 1","description":"Night Club 1 description","theme":"night-club","destination_id":10,"lat":"12.3346","log":"33.4455","created_at":"2017-01-17 00:35:56","updated_at":"2017-01-17 02:00:24"},{"id":5,"name":"Night Club 2","description":"Night Club 2 description","theme":"night-club","destination_id":10,"lat":"12.3346","log":"33.4455","created_at":"2017-01-17 00:35:56","updated_at":"2017-01-17 02:00:24"}]}
                    </div>
                </div>
                <br/><br/>
                <div class="links">
                 <a href="#">/api/place/4</a> <br/>
                    <div class="api-data">
                        {"place":{"id":4,"name":"Night Club 1","description":"Night Club 1 description","theme":"night-club","destination_id":10,"lat":"12.3346","log":"33.4455","created_at":"2017-01-17 00:35:56","updated_at":"2017-01-17 02:00:24"}}
                    </div>
                </div>
                <br/><br/>
                <div class="links">
                 <a href="#">/api/places/{theme}</a> theme=['restaurant','hotel','night-club','pub'] <br/>
                    <div class="api-data">
                        {"places":[{"id":4,"name":"Night Club 1","description":"Night Club 1 description","theme":"night-club","destination_id":10,"lat":"12.3346","log":"33.4455","created_at":"2017-01-17 00:35:56","updated_at":"2017-01-17 02:00:24"},{"id":5,"name":"Night Club 2","description":"Night Club 2 description","theme":"night-club","destination_id":10,"lat":"12.3346","log":"33.4455","created_at":"2017-01-17 00:35:56","updated_at":"2017-01-17 02:00:24"}]}
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
