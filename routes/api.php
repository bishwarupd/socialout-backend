<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


$api = app('Dingo\Api\Routing\Router');


$api->group([
    'version' => 'v1',
    'namespace' => 'App\Http\Controllers\api'
], function ($api) {

    $api->get('/places', 'PlaceController@index');
    $api->get('/places/{theme}', 'PlaceController@theme');
    $api->get('/place/{id}', 'PlaceController@show');
});

